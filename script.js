let allItems = document.querySelectorAll(".item");
let withPhoto = document.querySelectorAll(".with-photo");
let photoSection = document.querySelector(".photo-section");

window.addEventListener("load", addNewContainer);

function addNewContainer() {
  let counter = 0;
  allForm.forEach(el => {
    if (el.style.display === "none") {
      counter++;
    }
  });
  if (counter >= 9) {
    photoSection.insertAdjacentHTML('beforeend',
      `<div class="container">
  
        <div class="item item-horizontal--1">
          <form action="" class="form">
            <label class="image">
              <div class="crest"></div>
              <input type="file" accept="image/*">
            </label>
          </form>
  
  
          <div class="item--hover">
            <div class="item--hover__comment">
              <img src="icons/comment-icon.png" alt="comment-icon">
              <div class="icon--round comment-round-position"></div>
            </div>
            <div class="item--hover__likes">
              <div class="item--hover--dislike">
                <img src="icons/like-icon.png" alt="icon-dislike">
                <div class="icon--round dislike-round-position"></div>
              </div>
              <div class="item--hover--like">
                <img src="icons/like-icon.png" alt="icon-like">
                <div class="icon--round like-round-position"></div>
              </div>
            </div>
          </div>
        </div>
  
        <div class="item item-vertical">
          <form action="" class=" form">
          <label class="image">
            <div class="crest"></div>
            <input type="file" accept="image/*">
          </label>
          </form>
          <div class="item--hover">
            <div class="item--hover__comment">
              <img src="icons/comment-icon.png" alt="comment-icon">
              <div class="icon--round comment-round-position"></div>
            </div>
            <div class="item--hover__likes">
              <div class="item--hover--dislike">
                <img src="icons/like-icon.png" alt="icon-dislike">
                <div class="icon--round dislike-round-position"></div>
              </div>
              <div class="item--hover--like">
                <img src="icons/like-icon.png" alt="icon-like">
                <div class="icon--round like-round-position"></div>
              </div>
            </div>
          </div>
        </div>
  
        <div class="item">
          <form action="" class="form">
            <label class="image">
              <div class="crest"></div>
              <input type="file" accept="image/*">
            </label>
          </form>
          <div class="item--hover">
            <div class="item--hover__comment">
              <img src="icons/comment-icon.png" alt="comment-icon">
              <div class="icon--round comment-round-position"></div>
            </div>
            <div class="item--hover__likes">
              <div class="item--hover--dislike">
                <img src="icons/like-icon.png" alt="icon-dislike">
                <div class="icon--round dislike-round-position"></div>
              </div>
              <div class="item--hover--like">
                <img src="icons/like-icon.png" alt="icon-like">
                <div class="icon--round like-round-position"></div>
              </div>
            </div>
          </div>
        </div>
  
        <div class="item">
          <form action="" class="form">
            <label class="image">
              <div class="crest"></div>
              <input type="file" accept="image/*">
            </label>
          </form>
          <div class="item--hover">
            <div class="item--hover__comment">
              <img src="icons/comment-icon.png" alt="comment-icon">
              <div class="icon--round comment-round-position"></div>
            </div>
            <div class="item--hover__likes">
              <div class="item--hover--dislike">
                <img src="icons/like-icon.png" alt="icon-dislike">
                <div class="icon--round dislike-round-position"></div>
              </div>
              <div class="item--hover--like">
                <img src="icons/like-icon.png" alt="icon-like">
                <div class="icon--round like-round-position"></div>
              </div>
            </div>
          </div>
        </div>
  
        <div class="item">
          <form action="" class="form">
            <label class="image">
              <div class="crest"></div>
              <input type="file" accept="image/*">
            </label>
          </form>
          <div class="item--hover">
            <div class="item--hover__comment">
              <img src="icons/comment-icon.png" alt="comment-icon">
              <div class="icon--round comment-round-position"></div>
            </div>
            <div class="item--hover__likes">
              <div class="item--hover--dislike">
                <img src="icons/like-icon.png" alt="icon-dislike">
                <div class="icon--round dislike-round-position"></div>
              </div>
              <div class="item--hover--like">
                <img src="icons/like-icon.png" alt="icon-like">
                <div class="icon--round like-round-position"></div>
              </div>
            </div>
          </div>
        </div>
  
        <div class="item">
          <form action="" class="form">
            <label class="image">
              <div class="crest"></div>
              <input type="file" accept="image/*">
            </label>
          </form>
          <div class="item--hover">
            <div class="item--hover__comment">
              <img src="icons/comment-icon.png" alt="comment-icon">
              <div class="icon--round comment-round-position"></div>
            </div>
            <div class="item--hover__likes">
              <div class="item--hover--dislike">
                <img src="icons/like-icon.png" alt="icon-dislike">
                <div class="icon--round dislike-round-position"></div>
              </div>
              <div class="item--hover--like">
                <img src="icons/like-icon.png" alt="icon-like">
                <div class="icon--round like-round-position"></div>
              </div>
            </div>
          </div>
        </div>
  
        <div class="item">
          <form action="" class="form">
            <label class="image">
              <div class="crest"></div>
              <input type="file" accept="image/*">
            </label>
          </form>
          <div class="item--hover">
            <div class="item--hover__comment">
              <img src="icons/comment-icon.png" alt="comment-icon">
              <div class="icon--round comment-round-position"></div>
            </div>
            <div class="item--hover__likes">
              <div class="item--hover--dislike">
                <img src="icons/like-icon.png" alt="icon-dislike">
                <div class="icon--round dislike-round-position"></div>
              </div>
              <div class="item--hover--like">
                <img src="icons/like-icon.png" alt="icon-like">
                <div class="icon--round like-round-position"></div>
              </div>
            </div>
          </div>
        </div>
  
        <div class="item item-horizontal--2">
          <form action="" class="form">
            <label class="image">
              <div class="crest"></div>
              <input type="file" accept="image/*">
            </label>
          </form>
          <div class="item--hover">
            <div class="item--hover__comment">
              <img src="icons/comment-icon.png" alt="comment-icon">
              <div class="icon--round comment-round-position"></div>
            </div>
            <div class="item--hover__likes">
              <div class="item--hover--dislike">
                <img src="icons/like-icon.png" alt="icon-dislike">
                <div class="icon--round dislike-round-position"></div>
              </div>
              <div class="item--hover--like">
                <img src="icons/like-icon.png" alt="icon-like">
                <div class="icon--round like-round-position"></div>
              </div>
            </div>
          </div>
        </div>
  
        <div class="item">
          <form action="" class="form">
            <label class="image">
              <div class="crest"></div>
              <input type="file" accept="image/*">
            </label>
          </form>
          <div class="item--hover">
            <div class="item--hover__comment">
              <img src="icons/comment-icon.png" alt="comment-icon">
              <div class="icon--round comment-round-position"></div>
            </div>
            <div class="item--hover__likes">
              <div class="item--hover--dislike">
                <img src="icons/like-icon.png" alt="icon-dislike">
                <div class="icon--round dislike-round-position"></div>
              </div>
              <div class="item--hover--like">
                <img src="icons/like-icon.png" alt="icon-like">
                <div class="icon--round like-round-position"></div>
              </div>
            </div>
          </div>
        </div>
  
      </div>`);


  }

  allItems = document.querySelectorAll(".item");
  allImageInput = document.querySelectorAll("input[type=file]");
}

let popUp = document.getElementById("pop-up");
let popUpClose = document.getElementById("pop-up--close");
let imgOfPopUp = document.querySelector(".comments__img");
let allImageInput = document.querySelectorAll("input[type=file]");
let allItemsHover = document.querySelectorAll(".item--hover");
let allCommentsHover = document.querySelectorAll(".comment-round-position");
let allDislikesHover = document.querySelectorAll(".dislike-round-position");
let allLikesHover = document.querySelectorAll(".like-round-position");

let allForm = document.querySelectorAll(".form");
let allImg = document.querySelectorAll(".img");

let allContainers = document.querySelectorAll(".container");
let headOfSideBar = document.querySelector(".side-bar__head");
let dislike = document.querySelector(".icon--dislike");
let like = document.querySelector(".icon--like");

let parentOfForm = allForm.parentElement;

let valueOfLocalStorage = {};
let index;




for (let i = 0; i < allItems.length; i++) {
  console.log(allItems.length);
  // add 'background-image' to ITEM
  if (localStorage.getItem(`IMG_${i}`) !== null) {
    const parseOfValue = JSON.parse(localStorage.getItem(`IMG_${i}`));
    const imgSrc = parseOfValue.src;
    const backgroundImage = `background-image: url('img/${imgSrc}')`
    allItems[i].style = backgroundImage;
    allForm[i].style = "display: none";
    allItemsHover[i].classList.add("with-photo");
    withPhoto = document.querySelectorAll(".with-photo");

    // POP-UP appears
    if (allItems[i].addEventListener("click", function () {
        index = i;
        popUp.style.display = "block";
        imgOfPopUp.style = backgroundImage;

        // start --- add comments block
        valueOfComment = JSON.parse(localStorage.getItem(`Comment_${index}`));
        countOfLikes = JSON.parse(localStorage.getItem(`Counter_${index}`));

        // add quantity of likes
        if (countOfLikes.like) {
          like.firstElementChild.textContent = countOfLikes.like;
        }
        if (countOfLikes.dislike) {
          dislike.firstElementChild.textContent = countOfLikes.dislike;
        }

        // add quantity of comments
        if (valueOfComment) {
          headOfSideBar.textContent += valueOfComment.length;
        } else {
          headOfSideBar.textContent += 0;
        }

        // add comments
        if (valueOfComment) {
          valueOfComment.forEach(el => {
            sideBarComments.insertAdjacentHTML('afterbegin',
              `<div class="commentary">
                <div class="name-time">
                  <div class="nickname">By ${el.author}</div>
                  <div class="time-of-comment">${addTimeToComment(el.time)}</div>
                </div>
                <div class="new-comment">
                  ${el.comment}
                </div>
              </div>`
            )
          });
        }
        // end ---- add comments block
      }), false);
  }

  // add CURSOR: POINTER to image
  if (allForm[i].style.display === "none") {
    allItems[i].style.cursor = "pointer";
  }
}

// add SRC to local storage
window.addEventListener("load", addImageToLocalStorage);

function addImageToLocalStorage() {
  console.log(allImageInput);
  for (let i = 0; i < allImageInput.length; i++) {

    allImageInput[i].addEventListener("change", function () {
      const list = this.files;
      valueOfLocalStorage.src = list[0].name;

      let stringifyOfValue = JSON.stringify(valueOfLocalStorage);

      localStorage.setItem(`IMG_${i}`, `${stringifyOfValue}`);
      location.reload();
    }, false);
  }
}





// close POP-UP
popUpClose.addEventListener("click", popUpClosed);

function popUpClosed() {
  popUp.style.display = "none";
  location.reload();
}

// add comment to block & localStorage
const buttonSendComment = document.getElementById("comment-send");
const userName = document.getElementById("user-name");
const comment = document.getElementById("textarea-comment");
const formOfComment = document.getElementById("comments-form");
const sideBarComments = document.querySelector(".side-bar__comments")

let valueOfComment = [];
let date = {};

// add comment to localStorage & DOM
formOfComment.onsubmit = function (evt) {
  evt.preventDefault();
  let time = new Date();
  date.fullTime = time.getTime();
  date.year = time.getFullYear();
  date.month = time.getMonth();
  date.day = time.getDate();
  date.hours = time.getHours();
  date.minutes = time.getMinutes();

  valueOfComment = JSON.parse(localStorage.getItem(`Comment_${index}`));
  if (valueOfComment) {
    valueOfComment.push({
      author: userName.value,
      comment: comment.value,
      time: date
    });
  } else {
    valueOfComment = [];
    valueOfComment.push({
      author: userName.value,
      comment: comment.value,
      time: date
    });
  }

  let stringifyOfComment = JSON.stringify(valueOfComment);
  localStorage.setItem(`Comment_${index}`, stringifyOfComment);

  sideBarComments.insertAdjacentHTML('afterbegin',
    `<div class="commentary">
        <div class="name-time">
          <div class="nickname">By ${userName.value}</div>
          <div class="time-of-comment">${addTimeToComment(date)}</div>
        </div>
        <div class="new-comment">${comment.value}</div>
      </div>`)
  headOfSideBar.textContent = `Comments: ${valueOfComment.length}`;

  userName.value = '';
  comment.value = '';

}

function addTimeToComment(dateOfComment) {

  const now = new Date();
  const dayDifference = (now.getTime() - dateOfComment.fullTime) / 8.64e7;
  const timeOfComment = amOrPm(dateOfComment);

  function amOrPm() {
    if (dateOfComment.hours > 12) {
      return dateOfComment.hours - 12 + ':' + dateOfComment.minutes + ' PM';
    }
    return dateOfComment.hours + ':' + dateOfComment.minutes + ' AM';
  }

  if (dateOfComment.year == now.getFullYear() && dateOfComment.month == now.getMonth() && dateOfComment.day == now.getDate()) {
    return 'Today ' + timeOfComment;
  } else if (dayDifference < 2) {
    return 'Yesterday ' + timeOfComment;
  }
  return Math.floor(dayDifference) + ' days ago ' + timeOfComment;
}

let countOfLikes;

dislike.addEventListener("click", addNewLike);

function addNewLike() {
  dislike.classList.add("all-likes--active", "dislike--active");
  dislike.firstElementChild.style.border = "none";
  like.style.cursor = "auto";
  dislike.style.cursor = "auto";
  like.removeEventListener("click", addNewDislike);
}

dislike.addEventListener("click", counterOfDislike);

function counterOfDislike() {
  dislike.firstElementChild.textContent = addLikesToLocalStorage('dislike', index);
  dislike.removeEventListener("click", counterOfDislike);
  like.removeEventListener("click", counterOfLike);
}

like.addEventListener("click", addNewDislike);

function addNewDislike() {
  like.classList.add("all-likes--active", "like--active");
  like.firstElementChild.style.border = "none";
  like.style.cursor = "auto";
  dislike.style.cursor = "auto";
  dislike.removeEventListener("click", addNewLike);
}

like.addEventListener("click", counterOfLike);

function counterOfLike() {
  like.firstElementChild.textContent = addLikesToLocalStorage('like', index);
  like.removeEventListener("click", counterOfLike);
  dislike.removeEventListener("click", counterOfDislike);
}

function addLikesToLocalStorage(name, index) {
  if (localStorage.getItem(`Counter_${index}`)) {
    countOfLikes = JSON.parse(localStorage.getItem(`Counter_${index}`));
    countOfLikes[name] ? countOfLikes[name] += 1 : countOfLikes[name] = 1;
  } else {
    countOfLikes = {};
    countOfLikes[name] = 1;
  }

  const countOfLikesStringify = JSON.stringify(countOfLikes);
  localStorage.setItem(`Counter_${index}`, `${countOfLikesStringify}`);

  return countOfLikes[name];
}

// add counts on hover
allItems.forEach((el, i) => {
  if (allForm[i].style.display === "none") {
    valueOfComment = JSON.parse(localStorage.getItem(`Comment_${i}`));
    countOfLikes = JSON.parse(localStorage.getItem(`Counter_${i}`));

    valueOfComment ? allCommentsHover[i].textContent = valueOfComment.length :
      allCommentsHover[i].textContent = 0;

    if (countOfLikes) {
      countOfLikes.like ? allLikesHover[i].textContent = countOfLikes.like :
        allLikesHover[i].textContent = 0;

      countOfLikes.dislike ? allDislikesHover[i].textContent = countOfLikes.dislike :
        allDislikesHover[i].textContent = 0;
    } else {
      allLikesHover[i].textContent = 0;
      allDislikesHover[i].textContent = 0;
    }
  }
});

//add new CONTAINER 